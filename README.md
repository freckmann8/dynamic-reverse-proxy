# Dynamic Reverse Proxy

automated reverse proxy with letsencrypt 

## Installation instructions
* run `docker-compose up -d` in a dedicated directory for this git repo
  * note the docker-network name (e.g. `Creating network "reverse-proxy-net" with the default driver`)
  * important: the letsencrypt-companion automatically takes care about the certificate generation
    * **note** `docker-compose down -v` deletes the certificate and for repeatedly requesting new ones you may get temporarily blocked
* to get other docker-compose.yml to enter the reverse-proxy docker-network
  * add an external network at the end of the .yml:
```yml
networks:
  default:
    external:
      name: reverse-proxy-net
```
* to add services to the reverse proxy add the following lines to the service
```yml
environment:
      - VIRTUAL_HOST=test.example.com
      - LETSENCRYPT_HOST=test.example.com

```
* **note** you need to tell your service to communicate over https - if it's not set as default
### Multiple services 
* simplest way to add multiple services is to use subdomains
  * for domain 'example.com' possible service subdomains could be `test1.example.com` and `test2.exmaple.com`
* enter the corresponding subdomain names in the environment section of the service:
```yml
services:
  service1:
    environment:
      - VIRTUAL_HOST=test1.example.com
      - LETSENCRYPT_HOST=test1.example.com
  service2:
    environment:
      - VIRTUAL_HOST=test2.example.com
      - LETSENCRYPT_HOST=test2.example.com
``` 
[info: reverse-proxy from jwilder](https://github.com/nginx-proxy/nginx-proxy)